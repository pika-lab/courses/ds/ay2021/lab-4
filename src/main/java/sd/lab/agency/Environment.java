package sd.lab.agency;

import sd.lab.agency.fsm.AgentFSM;
import sd.lab.agency.fsm.impl.ExecutorBasedAgentFSM;
import sd.lab.agency.fsm.impl.ThreadBasedAgentFSM;
import sd.lab.agency.impl.ExecutorBasedEnvironment;
import sd.lab.agency.impl.MultiThreadedEnvironment;

import java.time.Duration;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

public interface Environment<A extends AgentFSM> {

    /**
     * Creates and registers an agent of type <code>A</code> (which must be a sub-type of {@link AgentFSM})
     * @param clazz is the <code>.class</code> object representing the type <code>A</code> of the to-be-created agent FSM
     * @param name is the local name of the to-be-created agent FSM
     * @param args are arguments to be forwarded to the constructor of class <code>A</code>
     * @return a reference to the newly created {@link AgentFSM} of type <code>A</code>
     * @see #registerAgent(AgentFSM)
     */
    A createAgent(Class<A> clazz, String name, Object... args);

    /**
     * Registers (i.e. adds) an already created agent to this {@link Environment}.
     * This is where the {@link AgentFSM#setAid(AID)} and {@link AgentFSM#setEnvironment(Environment)} methods are used
     * @param agent is a reference to the to-be-registered agent
     * @return the reference provided as argument
     */
    A registerAgent(A agent);

    /**
     * Retrieves a set of {@link AID}s referencing all the currently registered agents
     * @return
     */
    Set<AID> getAgents();

    /**
     * Gets the name of this {@link Environment}
     */
    String getName();

    /**
     * Blocks the caller until all agents terminate, or until a timeout of <code>duration</code> units of time expires
     * @param duration is an instance of {@link Duration}
     */
    void awaitAllAgents(Duration duration) throws InterruptedException, ExecutionException, TimeoutException;

    /**
     * Parses the provided string as an {@link AID}. However, if it contains a local {@link AID}, a global {@link AID}
     * is nevertheless created using {@link #getName()} as the environment name
     */
    AID aidOf(String localOrFullName);

    static <A extends ThreadBasedAgentFSM> Environment<A> multiThreaded(String name) {
        return new MultiThreadedEnvironment(name);
    }

    static <A extends ThreadBasedAgentFSM> Environment<A> multiThreaded() {
        return multiThreaded(null);
    }

    static <A extends ExecutorBasedAgentFSM> Environment<A> executorBased(String name, ExecutorService executorService) {
        return new ExecutorBasedEnvironment(name, executorService);
    }

    static <A extends ExecutorBasedAgentFSM> Environment<A> executorBased(String name) {
        return executorBased(name, Executors.newSingleThreadExecutor());
    }

    static <A extends ExecutorBasedAgentFSM> Environment<A> executorBased(ExecutorService executorService) {
        return executorBased(null, executorService);
    }

    static <A extends ExecutorBasedAgentFSM> Environment<A> executorBased() {
        return executorBased((String)null);
    }

}
