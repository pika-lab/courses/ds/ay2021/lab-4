package sd.lab.agency.impl;

import sd.lab.agency.Agent;
import sd.lab.agency.behaviour.Behaviour;
import sd.lab.agency.fsm.impl.ThreadBasedAgentFSM;

import java.util.*;

public abstract class AbstractAgent extends ThreadBasedAgentFSM implements Agent {

    private final Queue<Behaviour> toDoList = new LinkedList<>();
    private final Set<Behaviour> toBeRemoved = new HashSet<>();

    protected AbstractAgent(String name) {
        super(name);
    }

    @Override
    public Queue<Behaviour> getToDoList() {
        return toDoList;
    }

    @Override
    public final void onBegin() {
        setup();
    }

    @Override
    public void onRun() throws Exception {
        throw new Error("not implemented");
    }

    @Override
    public final void onEnd() {
        tearDown();
    }

    @Override
    public void setup() {
        // does nothing by default
    }

    @Override
    public void tearDown() {
        // does nothing by default
    }

    @Override
    public void addBehaviour(Collection<? extends Behaviour> behaviours) {
        if (behaviours.size() > 0) {
            toDoList.addAll(behaviours);
            resumeIfPaused();
        }
    }

    @Override
    public void removeBehaviour(Collection<? extends Behaviour> behaviours) {
        if (behaviours.size() > 0) {
            toBeRemoved.addAll(behaviours);
            resumeIfPaused(); // TODO notice this!
        }
    }
}
