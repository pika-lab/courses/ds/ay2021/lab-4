package sd.lab.agency;

import sd.lab.agency.behaviour.Behaviour;
import sd.lab.agency.fsm.AgentFSM;

import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;

public interface Agent extends AgentFSM {
    Queue<Behaviour> getToDoList();

    void setup();

    void tearDown();

    void addBehaviour(Collection<? extends Behaviour> behaviours);

    default void addBehaviour(Behaviour... behaviours) {
        addBehaviour(Arrays.asList(behaviours));
    }

    void removeBehaviour(Collection<? extends Behaviour> behaviours);

    default void removeBehaviour(Behaviour... behaviours) {
        removeBehaviour(Arrays.asList(behaviours));
    }

}
