package sd.lab.agency.fsm.impl;

import sd.lab.agency.AID;
import sd.lab.agency.Environment;
import sd.lab.agency.fsm.AgentFSM;
import sd.lab.utils.Action;

import java.time.Duration;
import java.util.EnumSet;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeoutException;

import static sd.lab.agency.fsm.impl.State.*;

public abstract class ThreadBasedAgentFSM implements AgentFSM {

    private final Semaphore resumeSignal = new Semaphore(0);
    private final CompletableFuture<Void> termination = new CompletableFuture<>();
    private AID aid;
    private Environment<?> environment;
    private volatile State state = State.CREATED;

    private final Thread thread = new Thread(() -> {
        throw new Error("not implemented");
    });

    /**
     * Executes a callback (e.g. {@link #onBegin()}, {@link #onRun()}, or {@link #onEnd()} ()}) in a protected way,
     * i.e., it calls {@link #onUncaughtError(Exception)} whenever an exception is thrown while running the <code>action</code>.
     *
     * Expected syntax example:
     * <code>
     *     protectExecution(ThreadBasedAgentFSM.this::onRun)
     * </code>
     *
     * @param action a function accepting 0 arguments and returning <code>void</code>, possibly throwing an {@link Exception}
     */
    private void protectExecution(Action<Exception> action) {
        try {
            action.execute();
        } catch (Exception e) {
            onUncaughtError(e);
        }
    }

    /**
     * Ensures the current {@link #state} of the FSM is equal to the provided <code>state</code>
     * @param state the only admissible {@link State}
     * @throws IllegalStateException if {@link #state} is not equal to <code>state</code>
     */
    protected final void ensureCurrentStateIs(State state) {
        ensureCurrentStateIsOneOf(state);
    }

    /**
     * Ensures the current {@link #state} of the FSM is contained among the provided <code>states</code>
     * @param states an {@link EnumSet} containing the admissible {@link State}s
     * @throws IllegalStateException if {@link #state} is not contained into <code>states</code>
     */
    protected final void ensureCurrentStateIsIn(EnumSet<State> states) {
        if (!currentStateIsIn(states)) {
            final RuntimeException e = new IllegalStateException("Illegal state: " + this.state + ", expected: " + states);
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Same functioning of {@link #ensureCurrentStateIsIn(EnumSet)}, except that admissible states are provided as
     * varargs
     */
    protected final void ensureCurrentStateIsOneOf(State state, State... states) {
        ensureCurrentStateIsIn(EnumSet.of(state, states));
    }

    /**
     * Atomically checks whether the current {@link #state} of this FSM is equal to <code>state</code>, returning a boolean value
     * instead of throwing an exception
     * @param state the only target {@link State}
     * @return <code>true</code> if {@link #state} is equal to <code>state</code>, or <code>false</code> otherwise
     */
    protected synchronized final boolean currentStateIs(State state) {
        return this.state == state;
    }

    /**
     * Atomically checks whether the current {@link #state} of this FSM is is contained among the provided <code>states</code>,
     * returning a boolean value instead of throwing an exception
     * @param states an {@link EnumSet} containing the target {@link State}s
     * @return <code>true</code> if {@link #state} is contained into <code>states</code>, or <code>false</code> otherwise
     */
    protected synchronized final boolean currentStateIsIn(EnumSet<State> states) {
        return states.contains(this.state);
    }

    /**
     * Same functioning of {@link #currentStateIsIn(EnumSet)}, except that admissible states are provided as varargs
     */
    protected synchronized final boolean currentStateIsOneOf(State state, State... states) {
        return currentStateIsIn(EnumSet.of(state, states));
    }

    /**
     * Atomically updates the current {@link #state} with the provided <code>state</code>
     * @param state the new {@link State} of this FSM
     */
    protected synchronized void setState(State state) {
        this.state = state;
    }

    public ThreadBasedAgentFSM(String name) {
        aid = AID.local(name);
    }

    @Override
    public final AID getAID() {
        return aid;
    }

    // This is assumed to be called be the environment alone, upon agent registering or creation
    @Override
    public final void setAid(AID aid) {
        this.aid = Objects.requireNonNull(aid);
    }

    public final Environment<?> getEnvironment() {
        return environment;
    }

    // This is assumed to be called be the environment alone, upon agent registering or creation
    public final void setEnvironment(Environment<?> environment) {
        ensureCurrentStateIs(State.CREATED);
        if (this.environment == null) {
            this.environment = Objects.requireNonNull(environment);
        }
    }

    public void onBegin() throws Exception {
        // it's a callback and it does nothing by default (must be inherited)
    }

    // TODO keep this abstract in order to force subclasses to provide some behaviour
    public abstract void onRun() throws Exception;

    @Override
    public void onEnd() throws Exception {
        // does nothing by default
    }

    @Override
    public void onUncaughtError(Exception e) {
        // by default, it simply print stacktrace and then stops the agent (can be overridden)
        e.printStackTrace();
    }

    @Override
    public final void start() {
        ensureCurrentStateIs(State.CREATED);
        throw new Error("not implemented");
    }

    @Override
    public final void resume() {
        ensureCurrentStateIs(PAUSED);
        throw new Error("not implemented");
    }

    @Override
    public final void pause() {
        ensureCurrentStateIsOneOf(STARTED, RUNNING, PAUSED);
        throw new Error("not implemented");
    }

    @Override
    public final void stop() {
        ensureCurrentStateIsOneOf(STARTED, RUNNING, PAUSED, STOPPED);
        throw new Error("not implemented");
    }

    @Override
    public final void restart() {
        ensureCurrentStateIsOneOf(STARTED, RUNNING, PAUSED, STOPPED);
        throw new Error("not implemented");
    }

    @Override
    public final void resumeIfPaused() {
        if (currentStateIs(PAUSED)) {
            resume();
        }
    }

    @Override
    public final void log(Object format, Object... args) {
        System.out.printf("[" + getAID() +"] " + format + "\n", args);
    }

    @Override
    public final void await(Duration duration) throws InterruptedException, ExecutionException, TimeoutException {
        throw new Error("not implemented");
    }

    @Override
    public final void await() throws InterruptedException, ExecutionException {
        throw new Error("not implemented");
    }
}

