package sd.lab.agency.fsm.impl;

import sd.lab.agency.AID;
import sd.lab.agency.Environment;
import sd.lab.agency.fsm.AgentFSM;

import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeoutException;

public class ExecutorBasedAgentFSM implements AgentFSM {

    public ExecutorBasedAgentFSM(String name) {
        throw new Error("not implemented");
    }

    @Override
    public void setAid(AID aid) {
        throw new Error("not implemented");
    }

    @Override
    public void onBegin() throws Exception {
        throw new Error("not implemented");
    }

    @Override
    public void onRun() throws Exception {
        throw new Error("not implemented");
    }

    @Override
    public void onEnd() throws Exception {
        throw new Error("not implemented");
    }

    @Override
    public void onUncaughtError(Exception e) {
        throw new Error("not implemented");
    }

    @Override
    public void start() {
        throw new Error("not implemented");
    }

    @Override
    public void stop() {
        throw new Error("not implemented");
    }

    @Override
    public void pause() {
        throw new Error("not implemented");
    }

    @Override
    public void resume() {
        throw new Error("not implemented");
    }

    @Override
    public void restart() {
        throw new Error("not implemented");
    }

    @Override
    public void resumeIfPaused() {
        throw new Error("not implemented");
    }

    @Override
    public AID getAID() {
        throw new Error("not implemented");
    }

    @Override
    public void await(Duration duration) throws InterruptedException, ExecutionException, TimeoutException {
        throw new Error("not implemented");
    }

    @Override
    public void await() throws InterruptedException, ExecutionException {
        throw new Error("not implemented");
    }

    @Override
    public Environment<?> getEnvironment() {
        throw new Error("not implemented");
    }

    @Override
    public void setEnvironment(Environment<?> environment) {
        throw new Error("not implemented");
    }

    @Override
    public void log(Object format, Object... args) {
        throw new Error("not implemented");
    }

    public ExecutorService getExecutorService() {
        throw new Error("not implemented");
    }

    public void setExecutorService(ExecutorService executorService) {
        throw new Error("not implemented");
    }
}
