package sd.lab.agency.fsm;

import sd.lab.agency.AID;
import sd.lab.agency.Environment;

import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public interface AgentFSM {
    /**
     * Gets the agent's {@link AID}
     */
    AID getAID();

    /**
     * Updates the agent's {@link AID}
     *
     * THIS IS ASSUMED TO BE CALLED BE THE ENVIRONMENT ALONE, UPON AGENT REGISTERING OR CREATION
     */
    void setAid(AID aid);

    void onBegin() throws Exception;
    void onRun() throws Exception;
    void onEnd() throws Exception;
    void onUncaughtError(Exception e);

    void start();
    void stop();
    void pause();
    void resume();
    void restart();
    void resumeIfPaused();

    void await(Duration duration) throws InterruptedException, ExecutionException, TimeoutException;
    void await() throws InterruptedException, ExecutionException;

    /**
     * Gets the agent's {@link Environment}
     */
    Environment<?> getEnvironment();

    /**
     * Gets the agent's {@link Environment}
     */
    void setEnvironment(Environment<?> environment);

    /**
     * Lets this agent print a log string by formetting <code>format.toString()</code> with the provided arguments
     */
    void log(Object format, Object... args);
}
