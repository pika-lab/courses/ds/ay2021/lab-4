package sd.lab.agency.behaviour.impl;

import sd.lab.agency.Agent;
import sd.lab.agency.behaviour.Behaviour;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

public class Wait implements Behaviour {

    private final Duration duration;
    private boolean started;
    private OffsetDateTime clock;
    private boolean ended;

    public Wait(Duration duration) {
        this.duration = Objects.requireNonNull(duration);
    }

    @Override
    public Behaviour deepClone() {
        return new Wait(duration);
    }

    @Override
    public void execute(Agent agent) {
        throw new Error("not implemented");
    }

    @Override
    public boolean isPaused() {
        throw new Error("not implemented");
    }

    @Override
    public boolean isOver() {
        throw new Error("not implemented");
    }

    private long getElapsedMillis() {
        return ChronoUnit.MILLIS.between(clock, OffsetDateTime.now());
    }
}
