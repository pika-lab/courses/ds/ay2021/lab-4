package sd.lab.agency.behaviour.impl;

import sd.lab.agency.Agent;
import sd.lab.agency.behaviour.Behaviour;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class Parallel implements Behaviour {

    private final LinkedList<Behaviour> subBehaviours = new LinkedList<>();
    private final TerminationCriterion terminationCriterion;
    private boolean shortCircuitEnd = false;

    public Parallel(TerminationCriterion terminationCriterion, Collection<Behaviour> bs) {
        this.terminationCriterion = terminationCriterion;
        if (bs.isEmpty()) throw new IllegalArgumentException();
        subBehaviours.addAll(bs);
    }

    public Parallel(TerminationCriterion terminationCriterion, Behaviour b, Behaviour... bs) {
        this.terminationCriterion = terminationCriterion;
        subBehaviours.add(b);
        subBehaviours.addAll(Arrays.asList(bs));
    }

    @Override
    public Behaviour deepClone() {
        return new Parallel(terminationCriterion, subBehaviours.stream().map(Behaviour::deepClone).collect(Collectors.toList()));
    }

    @Override
    public void execute(Agent agent) throws Exception {
        throw new Error("not implemented");
    }

    @Override
    public boolean isOver() {
        throw new Error("not implemented");
    }

    @Override
    public boolean isPaused() {
        throw new Error("not implemented");
    }

    public enum TerminationCriterion {ANY, ALL}

}
