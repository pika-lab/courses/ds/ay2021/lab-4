package sd.lab.agency.behaviour;

import sd.lab.agency.Agent;
import sd.lab.agency.behaviour.impl.DoWhile;
import sd.lab.agency.behaviour.impl.Parallel;
import sd.lab.agency.behaviour.impl.Sequence;
import sd.lab.agency.behaviour.impl.Wait;
import sd.lab.utils.Action;
import sd.lab.utils.Action1;

import java.time.Duration;
import java.util.function.Supplier;

@FunctionalInterface
public interface Behaviour {

    /// ACTUAL INTERFACE

    /**
     * Performs a single computational step for this {@link Behaviour}
     * @param agent the agent which is currently executing this {@link Behaviour}
     */
    void execute(Agent agent) throws Exception;

    /**
     * Checks whether this {@link Behaviour} is paused.
     * Paused behaviours are not executed, nor removed from the agent's to-do-list
     * @return <code>true</code> if the behaviour is paused
     */
    default boolean isPaused() {
        return false;
    }

    /**
     * Checks whether this {@link Behaviour} is over.
     * This check if performed by agents' internal scheduling AFTER EACH execution of {@link #execute(Agent)}.
     * Behaviours which are over are removed from the agent's to-do-list
     * @return <code>true</code> if the behaviour is over
     */
    default boolean isOver() {
        return true;
    }

    /**
     * Creates a fresh new copy of this {@link Behaviour}, which is ready to be executed, even if this behaviour has
     * already been executed. In case this behaviour is composite, sub-behaviours are cloned recursively.
     */
    default Behaviour deepClone() {
        return this;
    }

    /// STATIC FACTORIES

    static Behaviour of(Action<? extends Exception> action) {
        return agent -> action.execute();
    }

    static Behaviour of(Action1<Agent, ? extends Exception> action) {
        return action::execute;
    }

    static Behaviour sequence(Behaviour b, Behaviour... bs) {
        return new Sequence(b, bs);
    }

    static Behaviour allOf(Behaviour b, Behaviour... bs) {
        return new Parallel(Parallel.TerminationCriterion.ALL, b, bs);
    }

    static Behaviour anyOf(Behaviour b, Behaviour... bs) {
        return new Parallel(Parallel.TerminationCriterion.ANY, b, bs);
    }

    static Behaviour wait(Duration duration) {
        return new Wait(duration);
    }

    /// DEFAULT OPERATORS

    default Behaviour addTo(Agent agent) {
        agent.addBehaviour(this);
        return this;
    }

    default Behaviour removeFrom(Agent agent) {
        agent.removeBehaviour(this);
        return this;
    }

    default Behaviour andThen(Behaviour b, Behaviour... bs) {
        return new Sequence(this, b, bs);
    }

    default Behaviour andThen(Action<? extends Exception> action) {
        return andThen(Behaviour.of(action));
    }

    default Behaviour andThen(Action1<Agent, ? extends Exception> action) {
        return andThen(Behaviour.of(action));
    }

    default Behaviour repeatWhile(Supplier<Boolean> condition) {
        return DoWhile.of(this, condition);
    }

    default Behaviour repeatUntil(Supplier<Boolean> condition) {
        return repeatWhile(() -> !condition.get());
    }

    default Behaviour repeatForEver() {
        return repeatWhile(() -> true);
    }
}
